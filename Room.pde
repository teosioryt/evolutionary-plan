class Room {
  int area;
  int w, l;
  color c;
  int id;
  int corners;
  float ratio;
  float spread;

  HousePlan house;
  ArrayList<Room> connections;
  ArrayList<Tile> tiles;
  int maxX, maxY, minX, minY;

  Room (HousePlan house, int id) {
    this.id = id;
    this.c = color(random(255), 192, 192, 128);
    this.connections = new ArrayList();
    this.tiles = new ArrayList();
    this.house = house;
    this.maxX = -1;
    this.maxY = -1;
    this.minX = 10000;
    this.minY = 10000;
  }

  Room withColor(color c) {
    this.c = c;
    return this;
  }

  void addTile(Tile t) {
    tiles.add(t);
    if (t.x < minX) minX = t.x;
    if (t.y < minY) minY = t.y;
    if (t.x > maxX) maxX = t.x;
    if (t.y > maxY) maxY = t.y;
  }

  void analyze() {
      this.corners = this.countCorners();
      this.w = (this.maxX - this.minX + 1);
      this.l = (this.maxY - this.minY + 1);
      this.ratio = 1.0 - min(1.0 * this.w / this.l, 1.0 * this.l / this.w);
      this.spread = 1.0 - this.tiles.size() / (1.0 * this.w * this.l);
  }

  float fittness(float cornersW, float ratioW, float spreadW) {
      this.analyze();
      return cornersW * (this.corners - 4) + ratioW * this.ratio + spreadW * this.spread;
  }

  int countCorners() {
      int cornersOut = 0;
      for (Tile t : this.tiles) {
        int neighboursH = 0;
        int neighboursV = 0;
        for (Tile tn : this.house.getNeighbours(t))
          if (tn != null && tn.room != t.room) {
            if (!this.connections.contains(tn.room)) this.connections.add(tn.room);
            if (tn.y == t.y)
                neighboursH++;
            else
                neighboursV++;
          }
        if (neighboursH == 1 && neighboursV == 1) cornersOut++;
        if (neighboursH == 2 && neighboursV == 1) cornersOut += 2;
        if (neighboursH == 1 && neighboursV == 2) cornersOut += 2;
        if (neighboursH == 2 && neighboursV == 2) cornersOut += 4;
      }
      return 4 + (cornersOut - 4) * 2;
  }

  void display(int x0, int y0, int s) {
    if (this.id == 0) return;
    stroke(0);
    strokeWeight(2);
    for (Tile t : this.tiles)
      for (Tile tn : this.house.getNeighbours(t))
        if (tn != null && tn.room != t.room) {
          // if (!this.connections.contains(tn.room)) this.connections.add(tn.room);
          if (tn.y == t.y)
            line(x0 + (t.x+tn.x+1) / 2 * s, y0 + t.y * s,
              x0 + (t.x+tn.x+1) / 2 * s, y0 + (t.y+1) * s);
          else
            line(x0 + t.x * s, y0 + (t.y+tn.y+1) / 2 * s,
              x0 + (t.x+1) * s, y0 + (t.y+tn.y+1) / 2 * s);
        }

    fill(hue(c), 255, 192);
    ellipse(x0 + s * this.center().x, y0 + s * this.center().y, s * 0.25, s * 0.25);
    stroke(0, 192, 192);
    strokeWeight(1);
    for (Room neighbour: connections) {
        if (neighbour.id < this.id && neighbour.id != 0)
            line(x0 + s * this.center().x, y0 + s * this.center().y,
            x0 + s * neighbour.center().x, y0 + s * neighbour.center().y);
    }
  }

  PVector center() {
      return new PVector((maxX + minX + 1) * 0.5, (maxY + minY + 1) * 0.5);
  }
}
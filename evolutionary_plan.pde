import frazer.*;

Frazer frazer;

int plan_width = 20;
int plan_height = 20;
float chance = 0.5;
float[] definedRoomSizes = {100, 60, 50, 50, 50, 40, 40, 30, 20};
boolean keepEvolving = false;

HousePlan currentPlan;

void setup() {
  colorMode(HSB, 255, 255, 255);
  size(800, 800);
  frazer = new Frazer(this, 2000, plan_width * plan_height * 2 - plan_width - plan_height);

  currentPlan = new HousePlan(plan_width, plan_height);
  currentPlan.randomizeWalls(0.5);
  currentPlan.analyze();
}

void draw() {
  background(255);
  if (keepEvolving) evolve();
  currentPlan.display(100, 100, 30);
  Tile mot = currentPlan.mouseOver(mouseX, mouseY, 100, 100, 30);
  if(mot != null) text(String.format("Room %d: size %d, corners %d, ratio %f, spread %f",
                                     mot.room.id, mot.room.tiles.size(), mot.room.corners, 
                                     mot.room.ratio, mot.room.spread), 
                       25, 25);
}

void keyPressed() {
  switch(key) {
    case ' ':
      currentPlan.randomizeWalls(chance);
      break;
    case ',':
      chance -= chance > 0? 0.1 : 0;
      currentPlan.randomizeWalls(chance);
      break;
    case '.':
      chance += chance < 1? 0.1 : 0;
      currentPlan.randomizeWalls(chance);
      break;
    case 'q':
      keepEvolving = !keepEvolving;
      break;
    case 'e':
      Specimen best = frazer.evolve(100);
      currentPlan = new HousePlan(plan_width, plan_height, ((BitGenotype)best.getGenes()).getGenes());
      break;
  }
  println("chance: " + chance);
  currentPlan.analyze();
}

void evolve() {
  Specimen best = frazer.evolve(1);
  currentPlan = new HousePlan(plan_width, plan_height, ((BitGenotype)best.getGenes()).getGenes());
  currentPlan.analyze();
}

void mousePressed() {
  evolve();
  println("rooms count: " + currentPlan.rooms.size());
  println("rooms shape fitness: " + currentPlan.roomShapeFitness);
  println("rooms size fitness: " + currentPlan.roomSizeFitness);
}


float fitness(boolean[] genes) {
  HousePlan newPlan = new HousePlan(plan_width, plan_height, genes);
  return newPlan.analyze();
}
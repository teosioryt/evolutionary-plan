class Tile {
  boolean assigned;
  int x, y;
  Room room;

  Tile(int x, int y) {
    this.x = x;
    this.y = y;
    this.room = null;
    this.assigned = false;
  }

  void assign(Room room) {
    this.assigned = true;
    this.room = room;
    room.addTile(this);
  }

  void display(int x0, int y0, int s) {
    if (assigned)
      fill(this.room.c);
    else fill(128);
    rect(x0 + this.x * s, y0 + this.y * s, s, s);
  }
}
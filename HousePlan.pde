class HousePlan {
  int w, h;
  boolean[][] wallsV;
  boolean[][] wallsH;
  Tile[][] tiles;
  ArrayList<Room> rooms;
  float roomShapeFitness;
  float roomSizeFitness;


  HousePlan (int w, int h) {
    this.w = w;
    this.h = h;
    this.wallsV = new boolean[w-1][h];
    this.wallsH = new boolean[w][h-1];
    this.tiles = new Tile[w][h];
    this.rooms = new ArrayList();
  }

  HousePlan (int w, int h, boolean[] genes) {
    this(w, h);
    this.loadGenes(genes);
  }

  void loadGenes(boolean[] genes) {
    for (int x = 0; x < w-1; x++)
      for (int y = 0; y < h; y++)
        if (y == 0 || y == h-1)
          this.wallsV[x][y] = false;
        else
          this.wallsV[x][y] = genes[x * h + y]; //<>//

    for (int x = 0; x < w; x++) //<>//
      for (int y = 0; y < h-1; y++)
        if (x == 0 || x == w-1)
          this.wallsH[x][y] = false;
        else
          this.wallsH[x][y] = genes[x * (h-1) + y + (h-1)*w];
  }

  boolean[] randomizeWalls(float chance) {
    boolean[] genes = new boolean[w * h * 2 - w - h];
    for (int g = 0; g < genes.length; g++)
      genes[g] = random(1) < chance;
    this.loadGenes(genes);
    return genes;
  }

  float analyze() {
    int roomCount = 0;
    this.rooms.clear();
    Room currentRoom = new Room(this, roomCount).withColor(color(196));
    for (int x = 0; x < w; x++)
      for (int y = 0; y < h; y++)
        this.tiles[x][y] = new Tile(x, y);

    for (int x = 0; x < w; x++)
      for (int y = 0; y < h; y++)
        if (!this.tiles[x][y].assigned) {
          this.recursiveVisit(x, y, currentRoom);
          roomCount++;
          this.rooms.add(currentRoom);
          currentRoom = new Room(this, roomCount);
        } else continue;

    this.assignRoomColors(false);
    this.roomShapeFitness = this.assessRoomShapes(0.5, 0.3, 0.2);
    this.roomSizeFitness = this.assessRoomSizes(definedRoomSizes);
    return (this.roomShapeFitness + 99 * this.roomSizeFitness) / 100;
  }
  
  void assignRoomColors(boolean random) {
    boolean[] takenColors = new boolean[this.rooms.size()];
    for (int r = 0; r < this.rooms.size(); r++) takenColors[r] = false;
    
    for (Room r: this.rooms) {
      if (r.id > 0) {
        int cr = r.id;
        while (random && takenColors[cr])
          cr = int(random(this.rooms.size()));
        takenColors[cr] = true;
        r.c = color(map(cr, 0, this.rooms.size(), 0, 255), 192, 192, 128);
      }
    }
  }
  
  float assessRoomShapes(float cornersW, float ratioW, float spreadW) {
    float fitness = 0;
    for (Room r: this.rooms)
      if (r.id > 0)
        fitness += r.fittness(cornersW, ratioW, spreadW);
    fitness /= (this.rooms.size() - 1);
    if (this.rooms.size() == 1) return 1;
    return fitness;
  }
  
  float assessRoomSizes(float[] definedRoomSizes) {
    float[] roomSizes = new float[this.rooms.size() - 1];
    for (Room r: this.rooms)
      if (r.id > 0) 
        roomSizes[r.id-1] =  r.tiles.size();
    sort(roomSizes);
    sort(definedRoomSizes);
    float maxSize = this.w * this.h;
    float diffSum = abs(definedRoomSizes.length - roomSizes.length) * 100;
    for (int i = 0; i < roomSizes.length; i++) {
      diffSum += abs(roomSizes[i] - (i < definedRoomSizes.length ? definedRoomSizes[i] : 0)) / maxSize;
    }
    return diffSum / definedRoomSizes.length;
  }

  void recursiveVisit(int x, int y, Room r) {
    if (this.tiles[x][y].assigned) return;
    this.tiles[x][y].assign(r);
    if (x > 0 && !this.wallsV[x-1][y]) this.recursiveVisit(x-1, y, r);
    if (x < w-1 && !this.wallsV[x][y]) this.recursiveVisit(x+1, y, r);
    if (y > 0 && !this.wallsH[x][y-1]) this.recursiveVisit(x, y-1, r);
    if (y < h-1 && !this.wallsH[x][y]) this.recursiveVisit(x, y+1, r);
  }

  Tile getTile(int x, int y) {
    if (x >= 0 && x < this.tiles.length && y >= 0 && y < this.tiles[0].length)
      return this.tiles[x][y];
    else return null;
  }

  Tile[] getNeighbours(Tile t) {
    Tile[] neighbours = new Tile[4];
    neighbours[0] = this.getTile(t.x-1, t.y);
    neighbours[1] = this.getTile(t.x+1, t.y);
    neighbours[2] = this.getTile(t.x, t.y-1);
    neighbours[3] = this.getTile(t.x, t.y+1);
    return neighbours;
  }

  Tile mouseOver(int mouseX, int mouseY, int x0, int y0, int s) {
      int x = floor((mouseX - x0) / s);
      int y = floor((mouseY - y0) / s);
      return this.getTile(x, y);
  }

  void display(int x0, int y0, int s) {
    stroke(0);
    strokeWeight(0.1);

    for (int x = 0; x < w; x++)
      for (int y = 0; y < h; y++)
        this.tiles[x][y].display(x0, y0, s);
    strokeWeight(1);

    for (int x = 0; x < w-1; x++)
      for (int y = 0; y < h; y++)
        if (this.wallsV[x][y])
          line(x0 + (x+1) * s, y0 + y * s, x0 + (x+1) * s, y0 + (y+1) * s);

    for (int x = 0; x < w; x++) for (int y = 0; y < h-1; y++) if
      (this.wallsH[x][y]) line(x0 + x * s, y0 + (y+1) * s, x0 + (x+1) * s, y0 +
      (y+1) * s);
    strokeWeight(3);
    for (Room r : this.rooms) r.display(x0, y0, s);
  }
}